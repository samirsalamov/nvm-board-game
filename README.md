Getting Started
Solution is not hardcoded and I tried to follow Program to Interface (not depend on concrete implementation, rather an abstraction) and SOLID principles.

Solution is separated into parts:

- Data Reading
- Data Parsing
- Data Validation
- Track Finder
- Showing Result

In order to change logic of Track Finder at a runtime, has been used Strategy design pattern. I have created contracts for data reading, parsing and validating. So we can easily implement defined interfaces and change our solution easily.

Below is listed some examples:

Defined DataReader interface in order to change or add new data input sources. As a default I implemented a class named FileReader which reads data from file. In the future we can create DbReader class that data can be read from database and etc..

Defined InputParser interface in order to change or add new data input parsers. As a default I implemented a class named SingleMovementInputParser which parses given format like "MMMRMMLMRMLMRM". In the future we can create MultipleMovementInputParser class which could parse "3M2RM4LMM2RM" and etc..

Defined Validation interface in order to add new constraints or validation steps very easily. As a default I implemented a class named MovesValidation which defines move off board or not.

Defined MoveCommand interface in order to add other custom algorithms for movement. As a default I implemented a class named Mover that player could move right, left and forward. In the future we can create different movers which could be moved in another way.

So above structure and examples shows us we can add new feature to our solution or change existing solution very easily. Because all parts of solution loose coupled.

Our main class PlayerDirectionFinder has 2 methods: finds player full track (final position as well) and set initial position of player on the board. For finding full track could be passed different validations and movement strategy.

Let's start
In order to use/test system you should use below git command to clone project to your computer:

git clone git clone https://samirsalamov@bitbucket.org/samirsalamov/nvm-board-game.git
Otherwise you can download the whole project folder as .zip file from bitbucket page.

Prerequisites
Java 8
Maven 3

Building and running tests
mvn clean build

