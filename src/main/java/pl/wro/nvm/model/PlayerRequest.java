package pl.wro.nvm.model;

import java.util.Queue;

public class PlayerRequest {

    private String name;
    Queue<MoveDirection> moveDirections;

    public PlayerRequest(String name, Queue<MoveDirection> moveDirections) {
        this.name = name;
        this.moveDirections = moveDirections;
    }

    public String getName() {
        return name;
    }

    public Queue<MoveDirection> getMoveDirections() {
        return moveDirections;
    }
}
