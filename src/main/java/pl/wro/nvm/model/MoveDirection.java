package pl.wro.nvm.model;

public enum MoveDirection {
    M,
    LM,
    RM
}
