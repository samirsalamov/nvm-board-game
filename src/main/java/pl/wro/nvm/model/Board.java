package pl.wro.nvm.model;

public class Board {

    private int horizontalLength;
    private int verticalLength;


    public Board(int x, int y) {
        this.horizontalLength = x;
        this.verticalLength = y;
    }

    public boolean isMovementOufOffBoard(int x, int y) {
        return x < 0 || x >= horizontalLength || y >= verticalLength || y < 0;
    }
}
