package pl.wro.nvm.model;

public class Track {

    private int x;
    private int y;
    private FaceDirection direction = FaceDirection.North;

    public Track() {
    }

    public Track(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Track(int x, int y, FaceDirection direction) {
        this.x = x;
        this.y = y;
        this.direction = direction;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public FaceDirection getDirection() {
        return direction;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Track track = (Track) o;

        if (x != track.x) return false;
        if (y != track.y) return false;
        if (direction != track.direction) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = x;
        result = 31 * result + y;
        result = 31 * result + direction.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return x + " " + y + " " + direction;
    }
}