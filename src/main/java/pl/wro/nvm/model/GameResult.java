package pl.wro.nvm.model;

import java.util.LinkedList;
import java.util.List;

public class GameResult {

    private String playerName;
    private List<Track> trackPath = new LinkedList<>();

    public GameResult(String playerName) {
        this.playerName = playerName;
    }

    public String getPlayerName() {
        return playerName;
    }

    public List<Track> getTrackPath() {
        return trackPath;
    }

    public void addTrack(Track track) {
        trackPath.add(track);
    }

    public Track getLastTrack() {
        return trackPath.get(trackPath.size() - 1);
    }

    @Override
    public String toString() {
        StringBuilder trackPathBuilder = new StringBuilder(playerName).append(":").append(" ");
        for (Track track : trackPath) {
            trackPathBuilder.append(track.getX()).append(" ").append(track.getY()).append(" ").append(track.getDirection());
        }
        return trackPathBuilder.toString();
    }
}
