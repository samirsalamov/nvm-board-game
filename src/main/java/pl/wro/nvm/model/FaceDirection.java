package pl.wro.nvm.model;

public enum FaceDirection {
    North("N", 0, 1),
    East("E", 1, 0),
    South("S", 0, -1),
    West("W", -1, 0);

    private String shortName;

    private int stepOverXCoordinate;
    private int stepOverYCoordinate;

    FaceDirection(String shortName,int stepOverXCoordinate, int stepOverYCoordinate) {
        this.shortName = shortName;
        this.stepOverXCoordinate = stepOverXCoordinate;
        this.stepOverYCoordinate = stepOverYCoordinate;
    }

    public int getStepOverXCoordinate() {
        return stepOverXCoordinate;
    }

    public int getStepOverYCoordinate() {
        return stepOverYCoordinate;
    }

    @Override
    public String toString() {
        return shortName;
    }
}
