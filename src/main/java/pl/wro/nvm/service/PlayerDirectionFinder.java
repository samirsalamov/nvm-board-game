package pl.wro.nvm.service;

import pl.wro.nvm.command.MoveCommand;
import pl.wro.nvm.model.*;
import pl.wro.nvm.validator.Validation;

import java.util.Queue;

public class PlayerDirectionFinder {

    private Board board;
    private Track initialPosition;

    public PlayerDirectionFinder(Board board) {
        this.board = board;
    }

    public GameResult findPlayerTrack(
            PlayerRequest request, MoveCommand moveAction, Validation moveValidation) throws Exception {
        if (initialPosition == null) {
            initialPosition = new Track();
        }
        GameResult gameResult = new GameResult(request.getName());
        Queue<MoveDirection> shouldBeMoved = request.getMoveDirections();
        while (!shouldBeMoved.isEmpty()) {
            MoveDirection moveDirection = shouldBeMoved.poll();
            moveValidation.validate(board, initialPosition, moveDirection);
            Track completeTrack = moveAction.move(initialPosition, moveDirection);
            initialPosition = completeTrack;
            gameResult.addTrack(completeTrack);
        }
        return gameResult;
    }

    public void setInitialPositionInTheBoard(int x, int y, FaceDirection direction) {
        initialPosition = new Track(x, y, direction);
    }
}