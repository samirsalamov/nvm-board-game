package pl.wro.nvm.exception;

public class MoveOffException extends RuntimeException {

    public MoveOffException(String s) {
        super(s);
    }
}
