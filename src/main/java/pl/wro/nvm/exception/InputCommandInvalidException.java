package pl.wro.nvm.exception;

public class InputCommandInvalidException extends RuntimeException {

    public InputCommandInvalidException(String s) {
        super(s);
    }
}
