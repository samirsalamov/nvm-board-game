package pl.wro.nvm.command;

import pl.wro.nvm.model.MoveDirection;
import pl.wro.nvm.model.Track;

public interface MoveCommand {
    public Track move(Track track, MoveDirection direction);
}
