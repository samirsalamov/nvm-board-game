package pl.wro.nvm.command;

import static pl.wro.nvm.helper.FaceDirectionUtil.*;
import pl.wro.nvm.model.FaceDirection;
import pl.wro.nvm.model.MoveDirection;
import pl.wro.nvm.model.Track;

public class Mover implements MoveCommand {

    @Override
    public Track move(Track track, MoveDirection direction) {
        return createNewTrack(
                track,
                findNewDirection(track.getDirection(), direction));
    }

    private Track createNewTrack(Track previousTrack, FaceDirection newFaceDirection) {
        return new Track(
                previousTrack.getX() + newFaceDirection.getStepOverXCoordinate(),
                previousTrack.getY() + newFaceDirection.getStepOverYCoordinate(),
                newFaceDirection);
    }
}
