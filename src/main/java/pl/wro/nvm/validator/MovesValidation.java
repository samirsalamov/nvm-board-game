package pl.wro.nvm.validator;

import pl.wro.nvm.exception.MoveOffException;
import pl.wro.nvm.model.Board;
import pl.wro.nvm.model.FaceDirection;
import pl.wro.nvm.model.MoveDirection;
import pl.wro.nvm.model.Track;

import static pl.wro.nvm.helper.FaceDirectionUtil.*;

public class MovesValidation implements Validation {

    @Override
    public void validate(Board board, Track track, MoveDirection moveDirection) throws Exception {
        FaceDirection newFaceDirection = findNewDirection(track.getDirection(), moveDirection);
        int newX = track.getX() + newFaceDirection.getStepOverXCoordinate();
        int newY = track.getY() + newFaceDirection.getStepOverYCoordinate();
        if (board.isMovementOufOffBoard(newX, newY)) {
            throw new MoveOffException("[" + newX + ", " + newY + "] is out of board. So movement is not allowed!");
        }
    }
}
