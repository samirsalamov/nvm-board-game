package pl.wro.nvm.parser;

import pl.wro.nvm.exception.InputCommandInvalidException;
import pl.wro.nvm.model.MoveDirection;
import pl.wro.nvm.model.PlayerRequest;

import java.util.*;

public class SingleMovementInputParser implements InputParser {

    private static String ASSUME_RM_IS_R = "R";
    private static String ASSUME_LM_IS_L = "L";

    public List<PlayerRequest> parseInput(Map<String, String> input) throws InputCommandInvalidException {
        List<PlayerRequest> requests = new ArrayList<>();
        for (Map.Entry<String, String> entry : input.entrySet()) {
            requests.add(new PlayerRequest(entry.getKey(), extractDirections(entry.getValue())));
        }
        return requests;
    }

    private Queue<MoveDirection> extractDirections(String commandString) throws InputCommandInvalidException {
        Queue<MoveDirection> directions = new LinkedList<>();
        String  replacedCommandString = commandString.replaceAll("RM", ASSUME_RM_IS_R).replaceAll("LM", ASSUME_LM_IS_L);
        for (char ch : replacedCommandString.toCharArray()) {
            if (ch == 'R') {
                directions.add(MoveDirection.RM);
            } else if (ch == 'L') {
                directions.add(MoveDirection.LM);
            } else if (ch == 'M') {
                directions.add(MoveDirection.M);
            } else {
                throw new InputCommandInvalidException(commandString + " is wrong format!");
            }
        }
        return directions;
    }
}
