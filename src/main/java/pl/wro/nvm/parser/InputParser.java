package pl.wro.nvm.parser;

import pl.wro.nvm.exception.InputCommandInvalidException;
import pl.wro.nvm.model.PlayerRequest;

import java.util.*;

public interface InputParser {

    public List<PlayerRequest> parseInput(Map<String, String> input) throws InputCommandInvalidException;
}
