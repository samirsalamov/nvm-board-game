package pl.wro.nvm.helper;

import pl.wro.nvm.model.FaceDirection;
import pl.wro.nvm.model.MoveDirection;

public class FaceDirectionUtil {

    public static FaceDirection findNewDirection(FaceDirection currentFaceDirection, MoveDirection nextMove) {
        if (nextMove.equals(MoveDirection.M)) {
            return currentFaceDirection;
        } else if (nextMove.equals(MoveDirection.LM)) {
            return FaceDirection.values()[
                    currentFaceDirection.ordinal() == 0
                            ? FaceDirection.values().length - 1
                            : currentFaceDirection.ordinal() - 1];
        } else {
            return FaceDirection.values()[(currentFaceDirection.ordinal() + 1) % 4];
        }
    }

}
