package pl.wro.nvm.reader;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.*;

public class FileReader implements InputReader {

    private String filePath;

    public FileReader(String filePath) {
        this.filePath = filePath;
    }

    private List<String> readFileLines() throws IOException {
        final File file = new File(filePath);
        return Files.readAllLines(file.toPath());
    }

    @Override
    public Map<String, String> readInputs() throws IOException {
        Map<String, String> players = new LinkedHashMap<>();
        List<String> lines = readFileLines();
        for (String line : lines) {
            players.put(line.split(":")[0].trim(), line.split(":")[1].trim());
        }
        return players;
    }
}
