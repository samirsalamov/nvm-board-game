package pl.wro.nvm.reader;

import java.io.IOException;
import java.util.Map;

public interface InputReader {
    public Map<String, String> readInputs() throws IOException;
}
