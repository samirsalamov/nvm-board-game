package pl.wro.nvm;

import pl.wro.nvm.command.Mover;
import pl.wro.nvm.model.*;
import pl.wro.nvm.parser.InputParser;
import pl.wro.nvm.parser.SingleMovementInputParser;
import pl.wro.nvm.reader.FileReader;
import pl.wro.nvm.reader.InputReader;
import pl.wro.nvm.service.PlayerDirectionFinder;
import pl.wro.nvm.validator.MovesValidation;

import java.util.List;
import java.util.Map;

public class Main {

    public static void main(String[] args) throws Exception {

        InputReader reader = new FileReader("players\\players.txt");
        Map<String, String> requests = reader.readInputs();

        Board board = new Board(5, 5);
        InputParser parser = new SingleMovementInputParser();
        List<PlayerRequest> playerRequests = parser.parseInput(requests);

        PlayerDirectionFinder finder = new PlayerDirectionFinder(board);

        for (PlayerRequest request : playerRequests) {
            finder.setInitialPositionInTheBoard(0, 0, FaceDirection.North);
            GameResult gameResult = finder.findPlayerTrack(request, new Mover(), new MovesValidation());
            System.out.println("Name: " + gameResult.getPlayerName());
            System.out.println("Last Track: " + gameResult.getLastTrack());
            System.out.print("Track path: ");
            for (Track track : gameResult.getTrackPath()) {
                System.out.print(track + " | ");
            }
            System.out.println();
            System.out.println("----------------------------------------------------------------------------");
        }

    }
}
