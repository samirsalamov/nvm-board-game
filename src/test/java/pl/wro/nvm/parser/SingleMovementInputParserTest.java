package pl.wro.nvm.parser;

import static org.junit.Assert.*;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import pl.wro.nvm.exception.InputCommandInvalidException;
import static pl.wro.nvm.model.MoveDirection.*;

import pl.wro.nvm.model.MoveDirection;
import pl.wro.nvm.model.PlayerRequest;

import java.util.*;

public class SingleMovementInputParserTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();
    InputParser parser = new SingleMovementInputParser();

    @Test
    public void shouldThrowExceptionWhenInputCommandIsWrongFormat() {
        //given
        Map<String, String> input = new HashMap<>();
        input.put("Samir", "MMRMTLDADAD");

        thrown.expect(InputCommandInvalidException.class);
        thrown.expectMessage("MMRMTLDADAD is wrong format!");

        //when
        parser.parseInput(input);
    }

    @Test
    public void shouldReturnSinglePlayerRequestWhenInputCommandIsCorrectFormat() {

        //given
        Map<String, String> input = new HashMap<>();
        input.put("Player1", "MMMRMLMMMRMMMLM");

        //when
        List<PlayerRequest> result = parser.parseInput(input);

        //then
        assertTrue(result.size() == 1);
        assertTrue(result.get(0).getName().equals("Player1"));
        assertTrue(result.get(0).getMoveDirections().size() == 11);
        assertArrayEquals(
                result.get(0).getMoveDirections().toArray(),
                new MoveDirection[] {M, M, M, RM, LM, M, M, RM, M, M, LM}
        );
    }

    @Test
    public void shouldReturnMultiplePlayerRequestsWhenInputCommandIsCorrectFormat() {

        //given
        Map<String, String> input = new HashMap<>();
        input.put("Player1", "MMMRMLMMMRMMMLM");
        input.put("Player2", "MRMLMMRMMLM");
        //when
        List<PlayerRequest> result = parser.parseInput(input);

        //then
        assertTrue(result.size() == 2);

        assertTrue(result.get(0).getName().equals("Player2"));
        assertTrue(result.get(0).getMoveDirections().size() == 7);
        assertArrayEquals(
                result.get(0).getMoveDirections().toArray(),
                new MoveDirection[] {M, RM, LM, M, RM, M, LM}
        );

        assertTrue(result.get(1).getName().equals("Player1"));
        assertTrue(result.get(1).getMoveDirections().size() == 11);
        assertArrayEquals(
                result.get(1).getMoveDirections().toArray(),
                new MoveDirection[] {M, M, M, RM, LM, M, M, RM, M, M, LM}
        );

    }
}
