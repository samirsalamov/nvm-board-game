package pl.wro.nvm.helper;

import org.junit.Test;
import pl.wro.nvm.model.FaceDirection;

import static org.junit.Assert.*;
import static pl.wro.nvm.model.MoveDirection.*;
import static pl.wro.nvm.model.FaceDirection.*;

public class FaceDirectionUtilTest {

    @Test
    public void shouldReturnWestWhenMovingLMFromNorth() {

        //when
        FaceDirection west = FaceDirectionUtil.findNewDirection(North, LM);

        //then
        assertTrue(west.equals(west));
    }

    @Test
    public void shouldReturnNorthWhenMovingLMFromEast() {

        //when
        FaceDirection north = FaceDirectionUtil.findNewDirection(East, LM);

        //then
        assertTrue(north.equals(North));
    }

    @Test
    public void shouldReturnEastWhenMovingLMFromSouth() {

        //when
        FaceDirection east = FaceDirectionUtil.findNewDirection(South, LM);

        //then
        assertTrue(east.equals(East));
    }

    @Test
    public void shouldReturnSouthWhenMovingLMFromWest() {

        //when
        FaceDirection south = FaceDirectionUtil.findNewDirection(West, LM);

        //then
        assertTrue(south.equals(South));
    }

    @Test
    public void shouldReturnNorthWhenMovingRMFromWest() {

        //when
        FaceDirection north = FaceDirectionUtil.findNewDirection(West, RM);

        //then
        assertTrue(north.equals(North));
    }

    @Test
    public void shouldReturnEastWhenMovingRMFromNorth() {

        //when
        FaceDirection east = FaceDirectionUtil.findNewDirection(North, RM);

        //then
        assertTrue(east.equals(East));
    }

    @Test
    public void shouldReturnSouthWhenMovingRMFromNorth() {

        //when
        FaceDirection south = FaceDirectionUtil.findNewDirection(East, RM);

        //then
        assertTrue(south.equals(South));
    }

    @Test
    public void shouldReturnWestWhenMovingRMFromSouth() {

        //when
        FaceDirection west = FaceDirectionUtil.findNewDirection(South, RM);

        //then
        assertTrue(west.equals(West));
    }

    @Test
    public void shouldReturnSameWhenMovingMFromNorth() {

        //when
        FaceDirection north = FaceDirectionUtil.findNewDirection(North, M);

        //then
        assertTrue(north.equals(North));
    }

    @Test
    public void shouldReturnSameWhenMovingMFromEast() {

        //when
        FaceDirection east = FaceDirectionUtil.findNewDirection(East, M);

        //then
        assertTrue(east.equals(East));
    }

    @Test
    public void shouldReturnSameWhenMovingMFromSouth() {

        //when
        FaceDirection south = FaceDirectionUtil.findNewDirection(South, M);

        //then
        assertTrue(south.equals(South));
    }

    @Test
    public void shouldReturnSameWhenMovingMFromWest() {

        //when
        FaceDirection west = FaceDirectionUtil.findNewDirection(West, M);

        //then
        assertTrue(west.equals(West));
    }
}
