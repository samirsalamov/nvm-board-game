package pl.wro.nvm.model;

import static org.junit.Assert.*;
import org.junit.Test;

public class BoardTest {

    @Test
    public void shouldReturnTrueIfXNegative() {
        //given
        Board board = new Board(1, 1);

        //when
        boolean result = board.isMovementOufOffBoard(-1, 0);

        assertTrue(result == true);
    }

    @Test
    public void shouldReturnTrueIfYNegative() {
        //given
        Board board = new Board(1, 1);

        //when
        boolean result = board.isMovementOufOffBoard(0, -1);

        assertTrue(result == true);
    }

    @Test
    public void shouldReturnTrueIfXGreaterThanBoardHorizontalLength() {
        //given
        Board board = new Board(2, 2);

        //when
        boolean result = board.isMovementOufOffBoard(2, 1);

        assertTrue(result == true);
    }

    @Test
    public void shouldReturnTrueIfYGreaterThanBoardVerticalLength() {
        //given
        Board board = new Board(2, 2);

        //when
        boolean result = board.isMovementOufOffBoard(1, 2);

        assertTrue(result == true);
    }

    @Test
    public void shouldReturnFalseIfYLessThanBoardVerticalLength() {
        //given
        Board board = new Board(2, 2);

        //when
        boolean result = board.isMovementOufOffBoard(1, 1);

        assertTrue(result == false);
    }

    @Test
    public void shouldReturnFalseIfXLessThanBoardHorizontalLength() {
        //given
        Board board = new Board(2, 2);

        //when
        boolean result = board.isMovementOufOffBoard(1, 1);

        assertTrue(result == false);
    }

    @Test
    public void shouldReturnFalseIfXAndYBothLessThanBoardEdges() {
        //given
        Board board = new Board(3, 3);

        //when
        boolean result = board.isMovementOufOffBoard(1, 1);

        assertTrue(result == false);
    }

    @Test
    public void shouldReturnTrueIfXAndYBothNegative() {
        //given
        Board board = new Board(3, 3);

        //when
        boolean result = board.isMovementOufOffBoard(-1, -1);

        assertTrue(result == true);
    }

    @Test
    public void shouldReturnTrueIfXAndYBothGreaterThatBoardEdges() {
        //given
        Board board = new Board(3, 3);

        //when
        boolean result = board.isMovementOufOffBoard(3, 3);

        assertTrue(result == true);
    }
}
