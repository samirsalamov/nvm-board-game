package pl.wro.nvm.reader;

import org.junit.Test;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Map;

import static org.junit.Assert.*;

public class FileReaderTest {

    @Test
    public void shouldReturnEmptyListWhenFileContentIsEmpty() throws Exception {
        //given
        File file = createFakeFileWithContent("");
        FileReader reader = new FileReader(file.getPath());

        //when
        Map<String, String> results = reader.readInputs();

        //then
        assertTrue(results.size() == 0);
        file.delete();
    }

    @Test
    public void testFileReaderResponse() throws Exception {
        //given
        File file = createFakeFileWithContent("Samir: MMMRMLMMMM");
        FileReader reader = new FileReader(file.getPath());

        //when
        Map<String, String> results = reader.readInputs();

        //then
        assertTrue(results.keySet().contains("Samir"));
        assertTrue(results.values().contains("MMMRMLMMMM"));
        file.delete();
    }

    private File createFakeFileWithContent(String... contents) throws Exception {
        File file = new File("fake_file.txt");
        FileOutputStream out = new FileOutputStream(file);
        for (String content : contents) {
            out.write(content.getBytes());
        }
        out.flush();
        out.close();
        return file;
    }
}
