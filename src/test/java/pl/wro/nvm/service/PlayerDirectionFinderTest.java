package pl.wro.nvm.service;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import pl.wro.nvm.command.MoveCommand;
import pl.wro.nvm.command.Mover;
import pl.wro.nvm.exception.MoveOffException;
import pl.wro.nvm.model.*;
import pl.wro.nvm.validator.MovesValidation;
import pl.wro.nvm.validator.Validation;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

import static pl.wro.nvm.model.FaceDirection.East;
import static pl.wro.nvm.model.FaceDirection.North;
import static pl.wro.nvm.model.MoveDirection.*;

import static org.junit.Assert.*;

public class PlayerDirectionFinderTest {

    private Validation validator = new MovesValidation();
    private MoveCommand mover = new Mover();

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void shouldNotValidateIfMovementGoesOufOfBoard() throws Exception {
        //given
        thrown.expect(MoveOffException.class);
        thrown.expectMessage("[0, 2] is out of board. So movement is not allowed!");

        Board board = new Board(2, 2);
        PlayerDirectionFinder finder = new PlayerDirectionFinder(board);

        PlayerRequest request = new PlayerRequest("Drunk man", prepareMoveDirections(M, M, RM, LM, M));

        //when
        finder.findPlayerTrack(request, mover, validator);
    }

    @Test
    public void shouldReturnCorrectFinalPosition() throws Exception {

       //given
       Board board = new Board(5, 5);
       PlayerDirectionFinder finder = new PlayerDirectionFinder(board);

       PlayerRequest request = new PlayerRequest("Champion man", prepareMoveDirections(M, M, RM, LM, M));

       //when
       GameResult result = finder.findPlayerTrack(request, mover, validator);

       //then
       assertTrue(result.getLastTrack().getDirection().equals(North));
       assertTrue(result.getLastTrack().getX() == 1);
       assertTrue(result.getLastTrack().getY() == 4);
    }

    @Test
    public void shouldReturnCorrectTrackPath() throws Exception {
        //given

        Board board = new Board(10, 10);
        PlayerDirectionFinder finder = new PlayerDirectionFinder(board);

        PlayerRequest request = new PlayerRequest("ZigZag man", prepareMoveDirections(RM, LM, RM, LM, RM, LM, RM, LM));

        //when
        GameResult result = finder.findPlayerTrack(request, mover, validator);

        //then
        assertTrue(result.getTrackPath().size() == 8);
        assertTrue(result.getTrackPath().containsAll(
                Arrays.asList(
                        new Track(1, 0, East),
                        new Track(1, 1, North),
                        new Track(2, 1, East),
                        new Track(2, 2, North),
                        new Track(3, 2, East),
                        new Track(3, 3, North),
                        new Track(4, 3, East),
                        new Track(4, 4, North))));
    }

    private Queue<MoveDirection> prepareMoveDirections(MoveDirection... directions) {
        return new LinkedList<>(Arrays.asList(directions));
    }
}
