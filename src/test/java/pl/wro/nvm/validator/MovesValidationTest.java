package pl.wro.nvm.validator;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import pl.wro.nvm.exception.MoveOffException;
import pl.wro.nvm.model.Board;
import pl.wro.nvm.model.MoveDirection;
import pl.wro.nvm.model.Track;

public class MovesValidationTest {

    MovesValidation underTest = new MovesValidation();

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void wrongMoveValidationShouldNotPass() throws Exception {
        //given
        Board board = new Board(5, 5);
        Track track = new Track(0, 0);
        MoveDirection moveDirection = MoveDirection.LM;

        thrown.expect(MoveOffException.class);
        thrown.expectMessage("[-1, 0] is out of board. So movement is not allowed!");

        //when
        underTest.validate(board, track, moveDirection);
    }

    @Test
    public void correctMoveValidationShouldPass() throws Exception {
        //given
        Board board = new Board(2, 2);
        Track track = new Track(1, 1);
        MoveDirection moveDirection = MoveDirection.LM;

        //when
        underTest.validate(board, track, moveDirection);
    }
}
